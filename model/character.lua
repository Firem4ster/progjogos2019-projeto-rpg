
local Character = require 'common.class' ()

function Character:_init(spec)
  self.spec = spec
  self.hp = spec.max_hp
  self.atk = spec.atk
  self.type = spec.type
  self.alive = true
end

function Character:get_name()
  return self.spec.name
end

function Character:get_type()
  return self.spec.type
end

function Character:get_atk()
  return self.spec.atk
end

function Character:get_appearance()
  return self.spec.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:is_alive()
  return self.alive
end

function Character:take_damage(damage)
  self.hp = math.max(self.hp - damage, 0)
  if self.hp == 0 and self.alive then
    self:die()
  end
  return self.hp
end

function Character:die()
  self.alive = false
  print(self:get_name() .. " died!")
end

return Character

