
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local ActionState = 'action select'

local PlayerTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item', 'Run' }

function PlayerTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.enemies = {}
  self.cursor = nil
  self.target_enemy = 1
  self.target_cursor = nil
  self.menu = ListMenu(TURN_OPTIONS)
end

function PlayerTurnState:enter(params)
  self.character = params.current_character
  self.enemies = params.encounter
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function PlayerTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('turn_menu', self.menu)
end

function PlayerTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  self.cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', self.cursor)
end

-- mostra o cursor em cima do alvo
-- target é uma instancia de personagem
function PlayerTurnState:show_target_cursor(target)
  self:view():remove('target_cursor')
  local atlas = self:view():get('atlas')
  local enemy_atlas_instance = atlas:get(target)
  self.target_cursor = TurnCursor(enemy_atlas_instance)
  self:view():add('target_cursor', self.target_cursor)
end

function PlayerTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function PlayerTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function PlayerTurnState:on_keypressed(key)
  local option = TURN_OPTIONS[self.menu:current_option()]
  if ActionState == 'action select' then
    if key == 'down' then
      self.menu:next()
    elseif key == 'up' then
      self.menu:previous()
    elseif key == 'return' then
      -- entra em modo de ataque wooooo
      if option == 'Fight' then
        ActionState = 'target select'
        self:show_target_cursor(self.enemies[self.target_enemy])
      -- vai para o turno do proximo personagem
      else
        return self:pop({ action = option, character = self.character })
      end
    end

  elseif ActionState == 'target select' then
    if key == 'down' then
      self.target_enemy = math.min(self.target_enemy + 1, #self.enemies)
    elseif key == 'up' then
      self.target_enemy = math.max(self.target_enemy - 1, 1)
    end
    local enemy_instance = self.enemies[self.target_enemy]
    self:show_target_cursor(enemy_instance)

    -- da o dano, ou usa item, no alvo e passa pro proximo estado
    if key == 'return' then
      enemy_instance:take_damage(self.character:get_atk())
      self:view():remove('target_cursor')
      ActionState = 'action select'
      self.target_enemy = 1
      return self:pop({ action = option, character = self.character,
                        target = enemy_instance, enemies = self.enemies})
    end
  end
end

return PlayerTurnState
