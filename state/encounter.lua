
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local State = require 'state'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local ENCOUNTER = {}

local MESSAGES = {
  Fight = "%s attacked %s",
  Skill = "%s unleashed a skill",
  Item = "%s used an item",
}

function EncounterState:_init(stack)
  self:super(stack)
  self.turns = nil
  self.next_turn = nil
end

function EncounterState:enter(params)
  -- params.part e params.encounter
  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local n = 0
  local party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.next_turn = 1
  -- processa os personagens da party
  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    character.type = 'ally'
    self.turns[i] = character
    atlas:add(character, pos, character:get_appearance())
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  -- processa os personagens do encontro
  ENCOUNTER = params.encounter
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    character.type = 'enemy'
    self.turns[n + i] = character
    atlas:add(character, pos, character:get_appearance())
  end
  self:view():add('atlas', atlas)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  message:set("You stumble upon an encounter")
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('battlefield')
  self:view():remove('message')
end

function EncounterState:update(_)
  local current_character = self.turns[self.next_turn]
  self.next_turn = self.next_turn % #self.turns + 1
  local params = { current_character = current_character, encounter = ENCOUNTER}
  return self:push('player_turn', params)
end

function EncounterState:resume(params)
  -- termina o encontro atual
  if self.check_end_condition(params.enemies) or params.action == 'Run' then
    return self:pop()
  end
  local message
  if params.action == 'Fight' then
    message = MESSAGES[params.action]:format(params.character:get_name(),
                                             params.target:get_name())
  else
    message = MESSAGES[params.action]:format(params.character:get_name())
  end
  self:view():get('message'):set(message)
end

function EncounterState.check_end_condition(enemies)
  if enemies == nil then return false end
  for _, enemy in ipairs(enemies) do
    if enemy:is_alive() then
      return false
    end
  end
  print('cabou!')
  return true
end

return EncounterState
